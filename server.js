/** @format */

const express = require("express");
const app = express();
const http = require("http");
const { Server } = require("socket.io");
const path = require("path");
const cors = require("cors");
const { Socket } = require("socket.io");
app.use(cors());
app.use(express.static(path.join(__dirname, "client")));

app.get("/", (req, res) => {
    res.sendFile(__dirname + "/client/index.html");
});
const server = http.createServer(app);
const io = new Server(server, {
    cors: {
        origin: "http://localhost:3000",
        methods: ["GET", "POST"],
    },
});
let id_in_room = [];
io.on("connection", (socket) => {
    socket.on("join_room", (data) => {
        let rooms = io.sockets.adapter.rooms;
        let room = rooms.get(data);
        if (room == undefined) {
            socket.join(data);
            let datReturn = {
                number: 1,
                player: "mack",
                room: data,
            };
            id_in_room = [...id_in_room, { id: socket.id, room: data }];
            socket.emit("join_room", datReturn);
            socket.emit("change_number_in_room", 1);
            socket.to(data).emit("change_number_in_room", 1);
        } else if (room.size == 1) {
            socket.join(data);
            let datReturn = {
                number: 2,
                player: "kenji",
                room: data,
            };
            id_in_room = [...id_in_room, { id: socket.id, room: data }];

            socket.emit("join_room", datReturn);
            socket.emit("change_number_in_room", 2);
            socket.to(data).emit("change_number_in_room", 2);
            socket.emit("start_game", true);
            socket.to(data).emit("start_game", true);
        } else {
            socket.to(data).emit("join_room", false);
        }
    });
    socket.on("send_key_kenji", (data) => {
        console.log("send_key_kenji", data);
        socket.to(data.room).emit("get_key_kenji", data.pressed);
    });
    socket.on("send_key_mack", (data) => {
        console.log("send_key_mack", data);
        socket.to(data.room).emit("get_key_mack", data.pressed);
    });
    socket.on("resetGame", (check) => {});
    socket.on("disconnect", () => {
        let newArr = id_in_room.filter((e) => e.id != socket.id);
        if (newArr.length < id_in_room.length) {
            let room = id_in_room.filter((e) => e.id === socket.id);
            socket.to(room[0].room).emit("change_number_in_room", 1);
            id_in_room = [...newArr];
        }
    });
});
server.listen(3000, () => {
    console.log("server is run");
});
