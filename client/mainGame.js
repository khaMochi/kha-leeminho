/** @format */
const socket = io();
const $ = document.querySelector.bind(document);
const btn_join = $(".btn");
const input_id = $(".form_start_game > input");
const canvas = $("#main_game");
let currentControl;
const CANVAS_WIDTH = canvas.width;
const CANVAS_HEIGHT = canvas.height;
const ctx = canvas.getContext("2d", { alpha: false });
let infoGame = { isStart: false };
let currentAction = () => {};
let currentAction2 = () => {};
let noti_nember_b = $(".noti_nember > b");
let player_socket = "";
//mack kanji
let other_player = "";
const imgBackground = new Image();
imgBackground.src = "/img/background.png";

socket.on("start_game", () => {});
btn_join.addEventListener("click", (e) => {
    let value = input_id.value;
    if (value) {
        e.target.style.display = "none";
        socket.emit("join_room", value);
    }
});
socket.on("start_game", () => {});

class Player {
    constructor(objectFrames, x, ground, countIsAttack, typeAttack) {
        this.isOpenNormal = false;
        this.widthImage = 200;
        this.heightImage = 200;
        // khoang cach tu nhan vat den tren dau canvas = cho thua top +
        this.ground = ground;
        this.position = { x: x, y: this.ground };
        this.cutX = 0;
        this.cutY = 0;
        this.listFrames = { ...objectFrames };
        this.currentFrames = "";
        this.delay = 1;
        this.count = 0;
        this.lengthGame = 0;
        this.heigthJump = -70;
        this.attacking = false;
        this.changeY = 0;
        this.countIsAttack = countIsAttack;
        this.changeX = 0;
        this.pressed = {
            pressed: {
                a: false,
                d: false,
            },
            key: "",
        };
        this.typeAttack = typeAttack;
    }
    initFrames() {
        for (let i in this.listFrames) {
            let source = "/img/" + this.listFrames[i].img;
            this.listFrames[i].img = new Image();
            this.listFrames[i].img.src = source;
        }
        this.currentFrames = this.listFrames.Idle.img;
        this.delay = this.listFrames.Idle.delay;
        this.lengthGame = this.listFrames.Idle.length;
    }
    checkingAttack() {
        if (this.typeAttack === 2) {
            ctx.fillRect(this.position.x + 60, this.position.y + 200, 150, 75);
        } else {
            ctx.fillRect(this.position.x + 330, this.position.y + 170, 150, 75);
        }
    }
    handelChangeFrames() {
        this.count++;
        if (this.attacking && this.cutX === this.countIsAttack) {
            this.checkingAttack();
        }
        if (this.count % this.delay === 0) {
            if (this.cutX === this.lengthGame) {
                this.cutX = 0;
                if (this.attacking) {
                    this.attacking = false;
                    if (this.pressed.pressed.d) {
                        this.pressed.key = "d";
                    } else if (this.pressed.pressed.a) {
                        this.pressed.key = "a";
                    } else {
                        this.pressed.key = "normal";
                    }
                }
                this.count = 0;
            } else {
                this.cutX++;
            }
        }
    }
    controller(key) {
        let action = "";
        if (key) {
            if (this.pressed.pressed.a) {
                this.changeX = -5;
                action = "Run";
            } else if (this.pressed.pressed.d) {
                this.changeX = 5;
                action = "Run";
            }
            if (key === "w") {
                this.changeY === 0 && (this.changeY = -7);
            }
            if (this.changeY > 0) {
                if (this.position.y < this.ground) {
                } else {
                    this.position.y = this.ground;
                    this.changeY = 0;
                }
                action = "Fall";
            } else if (this.changeY < 0) {
                if (this.position.y > this.heigthJump) {
                } else {
                    this.position.y = this.heigthJump;
                    this.changeY = 7;
                    this.pressed.key = "normal";
                }
                action = "Jump";
            } else if (!action) {
                action = "Idle";
            }
            if (key === " ") {
                action = "Attack1";
                this.attacking = true;
            }
        }
        if (action && this.currentFrames !== this.listFrames[action].img) {
            this.count = 0;
            this.currentFrames = this.listFrames[action].img;
            this.lengthGame = this.listFrames[action].length;
            this.cutX = 0;
        }
    }
    draw() {
        this.position.x += this.changeX;
        this.position.y += this.changeY;
        const { x, y } = this.position;
        ctx.drawImage(
            this.currentFrames,
            this.cutX * this.widthImage,
            this.cutY,
            this.widthImage,
            this.heightImage,
            x,
            y,
            this.widthImage * 2.5,
            this.heightImage * 2.5
        );
        this.handelChangeFrames();
    }
}
const khaKenji = new Player(
    {
        Attack1: { img: "Kenji/Attack1.png", delay: 7, length: 3 },
        Attack2: { img: "Kenji/Attack2.png", delay: 5, length: 3 },
        Death: { img: "Kenji/Death.png", delay: 1, length: 6 },
        Fall: { img: "Kenji/Fall.png", delay: 1, length: 1 },
        Idle: { img: "Kenji/Idle.png", delay: 5, length: 3 },
        Jump: { img: "Kenji/Jump.png", delay: 1, length: 1 },
        Run: { img: "Kenji/Run.png", delay: 1, length: 7 },
        TakeHit: { img: "Kenji/Take hit.png", delay: 1, length: 2 },
    },
    460,
    160,
    1,
    2
);
khaKenji.initFrames();

const khaMack = new Player(
    {
        Attack1: { img: "Mack/Attack1.png", delay: 7, length: 5 },
        Attack2: { img: "Mack/Attack2.png", delay: 5, length: 3 },
        Death: { img: "Mack/Death.png", delay: 1, length: 5 },
        Fall: { img: "Mack/Fall.png", delay: 1, length: 1 },
        Idle: { img: "Mack/Idle.png", delay: 5, length: 7 },
        Jump: { img: "Mack/Jump.png", delay: 1, length: 1 },
        Run: { img: "Mack/Run.png", delay: 1, length: 7 },
        TakeHit: { img: "Mack/Take hit.png", delay: 1, length: 3 },
    },
    0,
    177,
    4,
    1
);
khaMack.initFrames();
ctx.fillStyle = "red";
socket.on("change_number_in_room", (data) => {
    noti_nember_b.innerText = data + "/2";
    if (data === 2) {
        currentAction = () => {
            khaKenji.changeX = 0;
            khaKenji.controller(khaKenji.pressed.key);
            khaKenji.draw();
        };
        currentAction2 = () => {
            khaMack.changeX = 0;
            khaMack.controller(khaMack.pressed.key);
            khaMack.draw();
        };
    }
});
socket.on("join_room", (result) => {
    infoGame = { ...result };
    console.log("info ", infoGame);
    infoGame.isStart = true;
    if (infoGame.number === 2) {
        currentControl = khaKenji;
        other_player = khaMack;
        socket.on(`get_key_mack`, (pressed) => {
            console.log("get key mack");
            other_player.pressed = pressed;
        });
    } else {
        currentControl = khaMack;
        other_player = khaKenji;
        socket.on(`get_key_kenji`, (pressed) => {
            console.log("get key kenji");
            other_player.pressed = pressed;
        });
    }
    window.addEventListener("keyup", (e) => {
        if (e.key === "a") {
            currentControl.pressed.pressed.a = false;
        } else if (e.key === "d") {
            currentControl.pressed.pressed.d = false;
        }
        socket.emit(`send_key_${infoGame.player}`, {
            pressed: currentControl.pressed,
            room: infoGame.room,
        });
    });
    window.addEventListener("keydown", (e) => {
        currentControl.pressed.key = e.key;
        if (e.key === "a") {
            currentControl.pressed.pressed.a = true;
        } else if (e.key === "d") {
            currentControl.pressed.pressed.d = true;
        }
        socket.emit(`send_key_${infoGame.player}`, {
            pressed: currentControl.pressed,
            room: infoGame.room,
        });
    });
});

const animation = () => {
    ctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
    ctx.drawImage(imgBackground, 0, 0);
    if (infoGame.isStart) {
        currentAction();
        currentAction2();
    }
    requestAnimationFrame(animation);
};
animation();
